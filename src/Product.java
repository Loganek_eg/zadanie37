public class Product {

    private double cena;
     private String name;
     private PRODUCT_TYPE type;
     private PRODUCT_TYPE pClass;

    public Product(double cena, String name, PRODUCT_TYPE type, PRODUCT_TYPE pClass) {
        this.cena = cena;
        this.name = name;
        this.type = type;
        this.pClass = pClass;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PRODUCT_TYPE getType() {
        return type;
    }

    public void setType(PRODUCT_TYPE type) {
        this.type = type;
    }

    public PRODUCT_TYPE getpClas() {
        return pClass;
    }

    public void setpClas(PRODUCT_TYPE pClas) {
        this.pClass = pClass;
    }

    @Override
    public String toString() {
        return name + "::" + cena + "::" + type + "::" + pClass;
    }
}
