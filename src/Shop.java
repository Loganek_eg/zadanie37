import java.util.List;
import java.util.Map;

public class Shop {


    Map<PRODUCT_TYPE, List<Product>> Productsmap;
    List<Product> Productslist;

    public Map<PRODUCT_TYPE, List<Product>> getProductsmap() {
        return Productsmap;
    }

    public void setProductsmap(Map<PRODUCT_TYPE, List<Product>> productsmap) {
        Productsmap = productsmap;
    }

    public List<Product> getProductslist() {
        return Productslist;
    }

    public void setProductslist(List<Product> productslist) {
        Productslist = productslist;
    }
}
