
        /*Zadanie 37:
Stworzymy aplikację, która będzie stanowić rejestr produktów w sklepie.
Zakładamy istnienie małego sklepiku, w którym musimy raz na jakiś czas
przeprowadzać inwentaryzację. Aby nie musieć spisywać wszystkich produktów
do notatnika, czy też innego dokumentu (np. excel'a) napiszemy aplikację,
która będzie dodawać produkty po ich wprowadzeniu w linii poleceń.
Aplikacja będzie obsługiwała następujące komendy:
dodaj NAZWA_PRODUKTU CENA TYP Z_JAKIEJ_POLKI
wylistuj_typ TYP
wylistuj_polka Z_JAKIEJ_POLKI
TYP - będzie enumem, będzie to typ produktu, nazwiemy go PRODUCT_TYPE.
Wyróżniamy kilka podstawowych typów (jeśli znacie więcej, to prosze śmiało dodać): FOOD, INDUSTRIAL, ALCOHOL
Z_JAKIEJ_POLKI będzie kolejnym enumem, nazwiemy go PRODUCT_CLASS i będzie reprezentować
typ klasy (np. coś z górnej półki, z średniej, z dolnej):
HIGH, MID, LOW
CENA jest ceną produktu wyrażoną w Double lub w Integer (zabezpiecz aplikację w taki
sposób aby przyjmowała oba typy parametrów)
NAZWA_PRODUKTU to po prostu String.
Chciałbym, aby powstała klasa Product, w której wszystkie powyższe parametry będą
polami tej klasy. Innymi słowy na produkt składa się coś, co ma nazwę, cenę, typ,
oraz wiemy z jakiej jest półki.
Następnie stworzymy klasę Magazine która będzie magazynem naszych produktów. Dodaj
 do klasy magazine pole, kolekcję produktów.
Proponuję zastosować Map<PRODUCT_CLASS, List<Product>>.
Przetestuj aplikację (napisz maina z obsługą komend).
Komendy do obsługi:
    - dodaj produkt (ze wszystkimi parametrami)
    - sprawdz czy produkt jest danego typu
    - sprawdz czy produkt jest na danej półce
    - sprawdz czy produkt istnieje
    - wylistuj produkty z konkretnej półki
    - wylistuj produkty konkretnego typu
    */
